%Question 4
diary HW2_Q4.out;
tolerance = 1e-9;
t=0;
%a. starting value of prices = [1,1,1]
tic
P = [1,1,1];
Pnew = [0,0,0];
while abs(P-Pnew)>tolerance

Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
t = t+1;
if t>=70 %maximum iteration
    break
end  
end
disp('Optimal Prices')
P
disp('Maximum demand')
Q
toc
%b. starting value of prices = [0,0,0]
tic
P = [0,0,0];
Pnew = [0,0,0];
while abs(P-Pnew)>tolerance
    % this does not run even once since at the beginning and the two prices
    % coincide. moreover abs(P-Pnew) is vector valued function, not a
    % scalar. 

Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
t = t+1;
if t>=70 %maximum iteration
    break
end  
end
disp('Optimal Prices')
P
disp('Maximum demand')
Q
toc
%c. starting value of prices = [0,1,2]
tic
P = [0,1,2];
Pnew = [0,0,0];
while abs(P-Pnew)>tolerance
    % this does not run even once since at the beginning and the two prices
    % coincide. moreover abs(P-Pnew) is vector valued function, not a
    % scalar. 
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
t = t+1;
if t>=70 %maximum iteration
    break
end  
end
disp('Optimal Prices')
P
disp('Maximum demand')
Q
toc%a. starting value of prices = [3,2,1]
tic
P = [3,2,1];
Pnew = [0,0,0];
while abs(P-Pnew)>tolerance

Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
t = t+1;
if t>=70 %maximum iteration
    break
end  
end
disp('Optimal Prices')
P
disp('Maximum demand')
Q
toc
diary off;