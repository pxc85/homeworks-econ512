diary HW2_Q2.out;
addpath('CEtools');
% Set options for Broyden
optset('broyden','showiters',0);
optset('broyden','maxit',100) ;
optset('broyden','tol',1e-9) ;
v=-1;
%firm's profit, Pi = p*q. d(Pi)/dp = q+p*dq/dp = q-p*q(1-q)
%next line has three firms profit functions.
Pi= @(P) [exp(v-P(1))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))) - ...
        P(1)*exp(v-P(1))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3)))...
        *(1-exp(v-P(1))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))));...
            exp(v-P(2))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))) - ...
            P(2)*exp(v-P(2))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3)))...
            *(1-exp(v-P(2))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))));...
                exp(v-P(3))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))) - ...
                P(3)*exp(v-P(3))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3)))...
                *(1-exp(v-P(3))/(1+exp(v-P(1))+exp(v-P(2))+exp(v-P(3))))];
            
% It should be not the profit, but the FOC on profit maximization. It
% produces all the mistakes

%a. starting value =[1,1,1]%
tic
P = broyden(Pi,[1;1;1]);

disp('Solutions of prices for starting value (1,1,1)')
P
disp('Maximum profit values')
Pi(P)
toc

%b. starting value = [0,0,0]%
tic
           
P = broyden(Pi,[0;0;0]);

disp('Solutions of prices for starting value (0,0,0)')
P
disp('Maximum profit functions')
Pi(P)
toc
%c. starting value = [0,1,2]%
tic
           
P = broyden(Pi,[0;1;2]);

disp('Solutions of prices for starting value (0,1,2)')
P
disp('Maximum profit functions')
Pi(P)
toc
%d. starting value [3,2,1]%
tic
  
P = broyden(Pi,[3;2;1]);

disp('Optimal prices for starting value (3,2,1)')
P
disp('Maximum profit functions')
Pi(P)
toc
diary off;
