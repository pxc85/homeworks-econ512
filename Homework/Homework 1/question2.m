%Create a 200x1 vector X containing evenly-spaced numbers between [-10, 20] and calculate the sum of the elements of the vector.
clear all;
diary quest2.out;
X = zeros(16,1)
X(1)=-10
for i=2:16
    X(i) = X(i-1)+2
end 
S=sum(X)
% X has dimension 16, it is not 200 evenly spaced points
diary off