clear all;
diary quest1.out;
X=[1 1.5 3 4 5 7 9 10]
Y1 = -2 + 0.5*X
Y2 = -2+0.5*X.^2
plot (X,Y1)
hold on
plot (X,Y2)
diary off;
