clear all;
diary ques6.out;
data = csvread('datahw1.csv');
Y = [data(:,5)]
X = [ones(size(Y)),data(:,3),data(:,4),data(:,6)]

% again, inv is a slow operation, use \ instead.
b = (inv(X'*X))*(X'*Y)
beta_0 = b(1)
beta_1=b(2)
beta_2=b(3)
beta_3=b(4)
se_b1 = std(X(:,2))/sqrt(numel(X(:,2)))
se_b2 = std(X(:,3))/sqrt(numel(X(:,3)))
se_b3 = std(X(:,4))/sqrt(numel(X(:,4)))
tvalue_b1 = beta_1/se_b1
tvalue_b2 = beta_2/se_b2
tvalue_b3 = beta_3/se_b3
pvalue_b1 = 1-tcdf(tvalue_b1,length(X(:,2))-1)
pvalue_b2 = 1-tcdf(tvalue_b2,length(X(:,3))-1)
pvalue_b3 = 1-tcdf(tvalue_b3,length(X(:,4))-1)
diary off;

