clear all;
diary quest5.out;
A = normrnd(10,5,5,3)
for i=1:5
    for j=1:3
        if A(i,j)<10
            A(i,j)=0
        end
        if A(i,j)>=10
            A(i,j)=1
        end
    end
end

% there is much easier way A = A>10
diary off;