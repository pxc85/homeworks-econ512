clear all;
diary quest3.out
A = [2 4 6 ; 1 7 5 ; 3 12 4]
B =  [-2 3 10]'
C = A'*B
D = inv(A'*A)*B

% If you break it down, E has different form than you were asked in the
% homework assignment
E = sum(A*B)
F = A([1,3] , 1:2)

% inv is slow, I suggest to use \ operator. take a look at the answer key
x = inv(A)*B
diary off;