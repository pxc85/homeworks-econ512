function mul = E(i,j)
load ('hw4data.mat');
X = data.X;
Y = data.Y;
Z = data.Z;
N = data.N;
T = data.T;
X = X';
Y = Y';
Z = Z';
mul = 1;
density = @(x) [1/(1+exp(-x))];
for t=1:20
    mul = mul*((density((i*X(j,t))))^(Y(j,t)))*((1-density((i*X(j,t))))^(1-Y(j,t)));
end
end