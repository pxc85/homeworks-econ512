addpath('CEtools');
load ('hw4data.mat');
X = data.X;
Y = data.Y;
Z = data.Z;
N = data.N;
T = data.T;


%% Constructing a single contribution to the likelihood function from individual i is calculated in the function E.m
%constructing pdf function for beta
%densityphi = @(x) [exp(-(1/2*pi)*((x-0.1)^2)/2)];
%outside multiplication: calculating likelihood function for the data set
beta0 = 0.1;
sigmabeta = 1;
likelihood1 = likelihoodguassian(beta0,sigmabeta);
% I think you codded the quadrature rule incorrectly, this calculates very
% long and does not produce the right answer. 
disp('value of likelihood function using guassian quadrateure')
likelihood1

%% Q2
likelihood2 = likelihoodmontecarlo(beta0,sigmabeta);
disp('value of likelihood function using monte carlo')
likelihood2
% this producec the wrong result. there are mistakes in emplementation. 
%% Q3
%starting value
ibeta0 = 2; 
sigmabeta0 = 2;
start = [ibeta0 sigmabeta0];

options = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[estimate1] = fminunc(@(beta01,sigmabeta1) (-1)*likelihoodguassian(beta01, sigmabeta1),start,options)
[estimate2] = fminunc(@(beta01,sigmabeta1) (-1)*likelihoodmontecarlo(beta01, sigmabeta1),start,options)
%  this does not give the correct result due to mistakes on early stages.
%  and also the optimization routine does not run either. 

