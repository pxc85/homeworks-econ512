addpath('CEtools');
%part a
%f = @(theta1, theta2, x) (theta2^theta1)*x^(theta1-1)*exp(-theta2*x)/gamma(theta1);
%Y1 = @(X,n) (sum(X)/n);
%Y2 = @(X,n) (exp((sum(ln(X))/n)));
%FOC1 = @(theta1, theta2, Y1,Y2,n) (n*log(theta2)+n*log(Y2)-psi(theta1));
%FOC2 = @(theta1, theta2, Y1,Y2,n) ((n*(theta1/theta2))-n*Y1);
%part b
%opttheta1 = @(opttheta2,Y1) (opttheta2*Y1);
%part c
%please see the file mleestimate.m
%part d
N = linspace(1.1,3,100);
for i = 1:100
func = @(theta1)(log(N(i))-log(theta1)-psi(theta1));
% You were not supposed to use generic optimization routines
f(i) = fsolve(func,2);
end
plot(N,f);
