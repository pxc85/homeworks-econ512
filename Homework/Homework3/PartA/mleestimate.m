function[theta1mle,theta2mle,variance] = mleestimate(X)
n = numel(X);
Y1 = (sum(X)/n);
Y2 = (exp((sum(log(X))/n)));
func = @(theta1) (log(theta1)-psi(theta1)-log(Y1)+log(Y2));
theta1mle = fsolve(func,2);
theta2mle = theta1mle/Y1;
variance = inv([(n/theta1mle - trigamma(theta1mle)) n/theta2mle ; n/theta2mle -n*theta1mle/theta2mle^2]);
end
