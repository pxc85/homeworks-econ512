clear all;
diary partb.out;
load ('../hw3.mat');
% please write down the correct path to data file
options = optimoptions(@fminunc,'Display','iter');
options1 = optimset('Display','iter','PlotFcns',@optimplotfval);
beta0 = [0,0,0,0,0,0];
fun = @(beta) -sum(-exp(X*beta')+y.*X*beta'-log(factorial(y)));
% you really don't need the factorial because it does not depend on
% variable parameters. it only careates computational troubles.
%q1
[betafminuncwithoutderivative,fval1,exitflag,output]= fminunc(fun, beta0,options)
%I could write the function which provides function and gradient but could
%not use it inside fminunc. The function is in a separate program
%"withgradient.m"

[betaneldermead,fval2,exitflag,output] = fminsearch(fun,beta0,options1)
% this thing apparently did not converge, see how I did it in answer key
%q3
residual = @(beta)(sum((y-exp(X*beta')).^2));
[betanlls,fval3,exitflag,output,grad,hessian] = fminunc(residual,beta0)
% other methods for nnls problem?
%q4
stderrorNLLS = sqrt(diag(inv(hessian)))
diary off;