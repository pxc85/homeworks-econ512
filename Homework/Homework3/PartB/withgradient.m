function [f,g] = withgradient(X,y)
% Calculate objective f
f = @(beta)(-sum(-exp(X*beta')+y.*X*beta'-log(factorial(y))));
R = y'*X;
if nargout > 1 % gradient required
    
    g = @(beta)([sum(X(:,1)).*exp(X*beta')-R(1);sum(X(:,2))*exp(X*beta')-R(2);sum(X(:,3))*exp(X*beta')-R(3);
        sum(X(:,4))*exp(X*beta')-R(4);sum(X(:,5))*exp(X*beta')-R(5);sum(X(:,6))*exp(X*beta')-R(6)]);
end